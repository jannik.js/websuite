import Dashboard from "./views/Dashboard";
import 'rsuite/dist/rsuite.min.css';
import { Link } from 'react-router-dom';

function App() {
    return (
        <div className="w-screen h-screen">
            <Dashboard/>
        </div>
    );
}

export default App;
