import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './assets/main.css';
import App from './App';
import Progress from './views/SnapshotProgress';
import reportWebVitals from './reportWebVitals';

// Register Service Worker
if('serviceWorker' in navigator) {
  navigator.serviceWorker.register('./sw.js');
};

ReactDOM.render(
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<App/>}/>
      <Route path="/progress/:id" element={<Progress/>}/>
    </Routes>
  </BrowserRouter>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
