import { useState, useRef } from 'react';
import { Button, Input, InputGroup, Uploader } from 'rsuite';
import Panel from '../components/Panel';
import { } from 'react-icons/md';

const CreateNewVm = () => {
    const [uploadValue, setUploadValue] = useState([]);
    const uploaderRef = useRef();

    const handleUpload = () => {
        uploaderRef.current.start();
    }

    const handleUploadChange = value => {
        console.log("handleUploadChange")
        console.log(value);
        setUploadValue(value);
    }
    return (
        <div className="min-h-full bg-gray-100">
            {/* Top Area */}
            <div className="flex flex-col bg-gray-100 text-gray-600 w-full pl-4 pt-2 pr-4 pb-2">
                <div className="flex flex-grow items-center justify-between flex-wrap">
                    {/* Lecture Name */}
                    <div className="flex flex-col xl:flex-row">
                        <h1 className="text-gray-600 font-bold text-3xl">Create a new VM</h1>
                    </div>
                </div>
            </div>

            <div className="w-full h-0.5 bg-gray-300"></div>

            {/* Main Area */}
            <div className="break-all">
                <div className="m-10">
                    <Panel title="Information" className="w-full">
                        <div className="mb-5">
                            If you have not created your own virtual machine yet, you can download a virtual machine as a template in the overview, customize it to your needs and then upload it using this wizard.
                        </div>
                        <div>
                            If you want to update the VM of an existing lecture, open the detail view of the existing VM and select 'Upload new VM version'. This ensures that existing permissions and links to lectures are retained.
                        </div>
                    </Panel>
                    <Panel title="Upload Image" className="w-full">
                        <InputGroup className="mb-4">
                                <InputGroup.Addon>
                                    Name
                                </InputGroup.Addon>
                                <Input placeholder=""/>
                        </InputGroup>
                        <Uploader draggable
                            action="https://webapi.schoenartz.com/images/0/versions/upload"
                            accept=".qcow2,.vmdk,.vhd,.png"
                            onChange={handleUploadChange}
                            autoUpload={false}
                            ref={uploaderRef}
                            fileList={uploadValue}
                            onProgress={() => { console.log("PROGRESS") }}
                            onRemove={() => { console.log("REMOVE") }}
                            onSuccess={() => { console.log("SUCCESS") }}
                            onReupload={() => { console.log("REUP") }}
                            onUpload={() => { console.log(uploadValue) }}
                            onError={() => { console.log("ERROR") }}
                        >
                            <div style={{ lineHeight: '200px' }}>Click or Drag files to this area to upload</div>
                        </Uploader>
                        <Button disabled={!uploadValue.length} onClick={handleUpload}>Upload</Button>
                    </Panel>
                </div>
            </div>
        </div>
    );
}

export default CreateNewVm;
