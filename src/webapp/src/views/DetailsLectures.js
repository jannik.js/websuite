import { useEffect, useState } from 'react';
import Button from '../components/Button';
import useRequestData from "../hooks/useRequestData";
import { MdModeEdit, MdDelete } from "react-icons/md";
import { VscRemoteExplorer } from 'react-icons/vsc';
import dataRequest from '../utils/dataRequest';
import axios from 'axios';
import GuacamoleDisplay from './GuacamoleDisplay';
import { Nav } from 'rsuite';
import Panel from '../components/Panel';
import config from '../config';

const DetailsLectures = ({ lectureID }) => {
    const [{ data: lecture, isLoading: lectureIsLoading }, { setId }] = useRequestData('lectures');
    const [owner, setOwner] = useState(null);
    const [activeTab, setActiveTab] = useState('overview');
    const [remoteToken, setRemoteToken] = useState(null);
    const [{ data: users }] = useRequestData('users', false);

    useEffect(() => {
        if (lectureID) setId(lectureID)
    }, [lectureID, setId]);

    // After image was loaded get the user from the owner id
    useEffect(() => {
        const getUser = async ownerId => {
            if (!ownerId) return '';
            const users = await dataRequest('users');
            let user = users.find(u => u.userId === ownerId);
            setOwner(user);
        }
        getUser(lecture.ownerId);
    }, [lectureIsLoading])

    const startRemoteSession = async () => {
        let url = '';
	if (config.api.hostname) url += config.api.hostname;
	if (config.api.port) url += `:${config.api.port}`;
        url += '/remote/' + lectureID;
        let response = await axios.get(url);
        setRemoteToken(response.data);
    }

    const CustomNav = ({ active, onSelect, ...props }) => {
        return (
          <Nav {...props} activeKey={active} onSelect={onSelect} className="bg-gray-100 flex flex-wrap">
            <Nav.Item eventKey="overview">Overview</Nav.Item>
            <Nav.Item eventKey="test">Some Tabs</Nav.Item>
            <Nav.Item eventKey="test2">Some More Tabs</Nav.Item>
            <Nav.Item eventKey="test3">Even More Tabs</Nav.Item>
          </Nav>
        );
      };

      const unixToDate = unixTimeStamp => {
        const date = new Date(unixTimeStamp * 1000);
        const year = date.getFullYear();
        const month = ('0' + (date.getMonth() + 1)).slice(-2);
        const day = date.getDate();
        const hours = date.getHours();
        let minutes = ('0' + date.getMinutes()).slice(-2);

        const formattedDate = day + '.' + month + '.' + year + ' ' + hours + ':' + minutes;
        return formattedDate;
    }

    const getUserFromId = id => {
        const user = users.find(user => user.userId === id);
        return user ? user.firstName + ' ' + user.lastName : undefined;
    }

    return (
        <div className="min-h-full bg-gray-100">
            {/* Top Area */}
            <div className="flex flex-col bg-gray-100 text-gray-600 w-full pl-4 pt-2 pr-4 pb-2">

                <div className="flex flex-row-reverse flex-wrap">
                    {/* VM IDs */}
                    <div className="flex flex-col items-end flex-shrink-0 text-xs font-mono	ml-4 -mt-1 -mr-2">
                        <span><span className="select-none">Lecture ID: </span>{lecture.lectureId}</span>
                    </div>
                </div>

                <div className="flex flex-grow items-center justify-between flex-wrap">
                    {/* Lecture Name */}
                     <div className="flex flex-col xl:flex-row">
                        <h1 className="text-gray-600 font-bold text-3xl">{lecture.lectureName}</h1>

                        {/* Owner */}
                        {owner &&
                            <h1 className="font-bold text-gray-500 self-end text-xl ml-4">by {owner.firstName} {owner.lastName}</h1>
                        }

                        {/* Dummy Loading for owner */}
                        {!owner &&
                            <div className="flex">
                                <div className="bg-gray-500 h-5 w-16 rounded animate-pulse"></div>
                                <div className="bg-gray-500 h-5 w-32 rounded animate-pulse ml-2"></div>
                            </div>
                        }
                    </div>

                    {/* Action Buttons */}
                    {!lectureIsLoading &&
                        <div className="flex self-end">
                            {remoteToken &&
                                <GuacamoleDisplay token={remoteToken} lectureID={lectureID}/>
                            }
                            <Button title="Remote" color="green" icon={VscRemoteExplorer} outline medium smallBorder onClick={startRemoteSession}/>
                            <Button title="Edit" color="orange" icon={MdModeEdit} outline medium smallBorder/>
                            <Button title="Delete" color="red" icon={MdDelete} outline medium smallBorder/>
                        </div>
                    }

                    {/* Dummy Loading Buttons */}
                    {lectureIsLoading &&
                        <div className="flex self-end">
                             <div className="bg-gray-500 h-5 w-32 rounded animate-pulse ml-2"></div>
                         </div>
                    }

                </div>
            </div>

            <div className="w-full h-0.5 bg-gray-300"></div>

            {/* Top menu bar */}
            <CustomNav appearance="subtle" active={activeTab} onSelect={setActiveTab} />

            {/* Main Area */}
            <div className="break-all">
                {!lectureIsLoading &&
                    <div className="p-4">
                        {/* Overview */}
                        {activeTab === 'overview' &&
                            <div className="flex flex-wrap">

                                <div className="flex flex-col">
                                    {/* Active circle */}
                                    <div className="rounded-full border-solid border-4 border-orange-400 h-32 w-32 flex items-center justify-center text-3xl font-bold m-4">
                                        <div className="flex flex-col items-center justify-center">
                                            {lecture.isEnabled
                                                ? <div className="text-green-500 font-semibold text-2xl">Active</div>
                                                : <div className="text-red-500 font-semibold text-2xl">Inactive</div>
                                            }
                                        </div>
                                    </div>

                                    {/* Uses circle */}
                                    <div className="rounded-full border-solid border-4 border-orange-400 h-32 w-32 flex items-center justify-center text-3xl font-bold m-4">
                                        <div className="flex flex-col items-center justify-center">
                                            {lecture.useCount}
                                            <div className="text-xs">times used</div>
                                        </div>
                                    </div>
                                </div>

                                {/* General information */}
                                <Panel className="w-auto flex-none h-auto bg-white" title="Information">
                                    <div className="grid grid-cols-4 gap-x-4 gap-y-4">

                                        {/* Owner */}
                                        <span className="text-gray-500">Owner</span>
                                        {owner &&
                                             <span className="text-gray-600 font-bold text-sm col-span-3">{owner.firstName} {owner.lastName}</span>
                                        }
                                        {!owner &&
                                            <div className="flex mb-2 col-span-3">
                                                <div className="bg-gray-500 h-4 w-10 rounded animate-pulse"></div>
                                                <div className="bg-gray-500 h-4 w-16 rounded animate-pulse ml-1"></div>
                                            </div>
                                        }

                                        {/* Start time */}
                                        <span className="text-gray-500">Start</span>
                                        <span className="text-gray-600 font-bold text-sm col-span-3">{unixToDate(lecture.startTime)}</span>

                                        {/* End time */}
                                        <span className="text-gray-500">End</span>
                                        <span className="text-gray-600 font-bold text-sm col-span-3">{unixToDate(lecture.endTime)}</span>

                                        {/* Creation time */}
                                        <span className="text-gray-500">Created on</span>
                                        <span className="text-gray-600 font-bold text-sm col-span-3">{unixToDate(lecture.createTime)}</span>

                                        {/* Changed on */}
                                        <span className="text-gray-500">Changed on</span>
                                        <span className="text-gray-600 font-bold text-sm col-span-3">{unixToDate(lecture.updateTime)}</span>

                                        {/* Changed by */}
                                        <span className="text-gray-500">Changed by</span>
                                        <span className="text-gray-600 font-bold text-sm col-span-3">{getUserFromId(lecture.updaterId)}</span>

                                        {/* Last used */}
                                        <span className="text-gray-500">Last use</span>
                                        <span className="text-gray-600 font-bold text-sm col-span-3">{unixToDate(lecture.lastUsed)}</span>

                                        {/* Image version ID */}
                                        <span className="text-gray-500">Image Version ID</span>
                                        <span className="text-gray-600 font-bold text-sm col-span-3">{lecture.imageVersionId}</span>

                                        {/* Image base ID */}
                                        <span className="text-gray-500">Image Base ID</span>
                                        <span className="text-gray-600 font-bold text-sm col-span-3">{lecture.imageBaseId}</span>

                                    </div>
                                </Panel>

                                {/* Description */}
                                <Panel className="flex-grow h-auto bg-white" title="Description">
                                    <pre>
                                        {lecture.description}
                                    </pre>
                                </Panel>
                            </div>
                        }

                        {/* Something */}
                        {activeTab === 'test' &&
                            <div className="flex flex-wrap">
                                <div>{remoteToken}</div>
                                <div>{JSON.stringify(lecture)}</div>
                            </div>
                        }

                    </div>
                }
            </div>
        </div>
    );
}

export default DetailsLectures;


