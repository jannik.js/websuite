import { useState, useEffect } from 'react';
import List from '../components/List';
import Tab from '../components/Tab';
import Button from '../components/Button';
import VMsView from './DetailsVMs';
import LecturesView from './DetailsLectures';
import VmListElement from './VmListElement';
import LectureListElement from './LectureListElement';
// import Table from '../components/Table';
import SelectableTable from './SelectableTable';

import { IoSchoolOutline } from "react-icons/io5";
import { AiOutlineDesktop } from "react-icons/ai";
import { MdAdd, MdList } from "react-icons/md";
import { ImTable } from "react-icons/im";
import dataRequest from '../utils/dataRequest';
import Divider from '../components/Divider';
import CreateNewVm from './CreateNewVm';


const TabList = ({ className = "", onChooseElement = () => {} }) => {
    const [activeTab, setActiveTab] = useState(null);
    const [listData, setListData] = useState([]);
    const [fetchingData, setFetchingData] = useState(true);
    const [returnKey, setReturnKey] = useState(null);
    const [currentKey, setCurrentKey] = useState(null);

    const osLogos = [
        "debian_logo.png",
        "dos_logo.png",
        "opensuse_logo.png",
        "other_linux_logo.png",
        "ubuntu_logo.png",
        "windows_7_logo.png",
        "windows_10_logo.png",
        "windows_2000_professional_logo.png",
        "windows_nt_4_logo.png",
        "windows_vista_logo.png",
        "windows_xp_logo.png",
    ];

    useEffect(() => {
        if (currentKey) {
            if (currentKey === 'vmTable') {
                // Set main container to the vm multiselect Table
                // onChooseElement(<Table headers={['imageName', 'owner', 'virtId']} data={listData} multiselect/>);
                onChooseElement(
                    <SelectableTable data={listData}/>
                );
            } else if (returnKey === 'imageBaseId') {
                // Set main container view to VMsView
                onChooseElement(<VMsView vmID={currentKey}/>);
            } else if (returnKey === 'lectureId') {
                onChooseElement(<LecturesView lectureID={currentKey} />);
            }
        }
    }, [currentKey]);

    const selectTab = tabId => {
        setActiveTab(tabId);
        if (tabId === 'images') {
            // Fetch and process the data
            getVirtualMachines();
        } else if (tabId === 'lectures') {
            getLectures();
        }
    }

    const getVirtualMachines = async () => {
        setFetchingData(true);
        const images = await dataRequest('images');
        const users = await dataRequest('users');
        const operatingsystems = await dataRequest('operatingsystems');
        images.map(vm => {
            // Replace virtualizer with a logo
            const vmType = vm['virtId'];
            let src = `/img/virtualizer/${vmType}_logo.png`;

            vm['virtId_html'] = (<object className="h-4" data={src} type={src && "image/png"}>
                <div className="w-auto">{vm['virtId']}</div>
            </object>);

            // Map ownerId to the owner's name
            const owner = users.find(user => user.userId === vm['ownerId']);
            vm['owner'] = owner.firstName + ' ' + owner.lastName;

            // Map operating system id to the os name
            const os = operatingsystems.find(os => os.osId === vm['osId']);
            vm['os'] = os.osName;

            const cutIndex = vm['os'].indexOf('(') !== -1 ? vm['os'].indexOf('(') - 1 : vm['os'].length;

            // Second replace all is a special space character because of Windows NT
            let osSrc = vm['os'].slice(0, cutIndex).replaceAll(' ', '_').replaceAll(' ', '_').toLowerCase() + '_logo.png';

            if (osLogos.includes(osSrc)) osSrc = '/img/os/' + osSrc;
            else osSrc = undefined;

            vm['os_html'] = osSrc ? (<object className="h-4" data={osSrc} type={osSrc && "image/png"}>
                <div className="w-auto">{vm['os']}</div>
            </object>) : '';

            vm['fileSize_formatted'] = formatBytes(vm['fileSize'], 2);

            return vm;
        });

        if (!images.error) setListData(images);
        else {
            console.log(images.error);
            setListData([]);
        }
        setReturnKey("imageBaseId");
        setFetchingData(false);
    }

    const getLectures = async () => {
        setFetchingData(true);
        const lectures = await dataRequest('lectures');

        console.log(lectures)

        lectures.map(lecture => {
            const startTime = lecture.startTime * 1000;
            const endTime = lecture.endTime * 1000;

            let date = new Date(startTime);
            lecture.startTime = dateToString(date);
            date = new Date(endTime);
            lecture.endTime = dateToString(date);

            // Red color, if endtime is in the past
            const currentTime = Date.now();
            if (currentTime > parseInt(endTime)) lecture.classNames = { 'endTime': 'text-red-500' };

            return lecture;
        });

        if (!lectures.error) setListData(lectures);
        else {
            console.log(lectures.error);
            setListData([]);
        }
        setReturnKey("lectureId");
        setFetchingData(false);
    }

    const formatBytes = (bytes, decimals = 2) => {
        if (bytes === 0) return '0 Bytes';

        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        const i = Math.floor(Math.log(bytes) / Math.log(k));

        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    const dateToString = (date) => {
        const tmpDate = twoDigits(date.getDate()) + '.' + twoDigits(date.getMonth() + 1) + '.' + date.getFullYear();
        const tmpTime = twoDigits(date.getHours()) + '.' + twoDigits(date.getMinutes());
        return tmpDate + ' ' + tmpTime;
    }

    const twoDigits = (digits) => {
        return ('0' + digits).slice(-2);
    }

    const addButtonClick = event => {
        if (activeTab === 'images') {
            // Set main content to the create new image view.
            onChooseElement(<CreateNewVm/>);
        } else if (activeTab === 'lectures') {

        }
    }

    return (
        <div className={`flex flex-col w-full h-full overflow-auto ${className}`}>
            {/* Tabs */}
            <div className="bg-gray-50 flex flex-row justify-around items-center w-full">
                <Tab icon={<AiOutlineDesktop/>} title="Images" id="images" selected={activeTab} onClick={id => selectTab(id)}/>
                <Tab icon={<IoSchoolOutline/>} title="Lectures" id="lectures" selected={activeTab} onClick={id => selectTab(id)}/>
            </div>

            {/* List */}
            <div className="flex flex-col w-full h-full overflow-auto">
                {activeTab &&
                    <List data={listData} onClick={returnKey => setCurrentKey(returnKey)} onClickReturnKey={returnKey} filter loading={fetchingData}
                        listElement={activeTab === 'images' ? VmListElement : LectureListElement}
                        listOfFilterValues={
                            activeTab === 'images' ? ["owner", "virtId", "os"]
                            : activeTab === 'lectures' ? ["lectureId"]
                            : []
                        }
                    />
                }
            </div>

            <Divider/>

            {/* Buttons */}
            <div className="bg-gray-50 pl-4 pr-4 pt-4 pb-4 flex justify-center items-center w-full">
                {activeTab &&
                    <div className="flex flex-row">
                        <Button title="Add" icon={MdAdd} color="green" full outline onClick={addButtonClick}/>
                        {activeTab === 'images' &&
                            <Button title="" icon={ImTable} iconStroke="0" color="gray" iconColor="blue" outline onClick={() => { setCurrentKey('vmTable'); }}/>
                        }
                    </div>
                }
            </div>
        </div>
    );
}

export default TabList;
