import React from 'react';
import Badge from '../components/Badge';

const LectureListElement = ({ data, changeFilter, dummy }) => {
    const lecture = data;

    return (
        <div>
            {!dummy &&
                <div>
                    <div className="font-semibold text-sm text-gray-800 truncate ml-2">
                        {lecture.lectureName}
                    </div>
                    <div className="flex flex-start">
                        <div className="text-xs text-gray-500 hover:text-gray-100 ml-2" onClick={() => {}}>
                            {lecture.description}
                        </div>
                    </div>
                    <div className="flex flex-row mt-2">
                        {/*
                        <Badge onClick={() => {}} logo={lecture} title={lecture} hoverColor="bg-gray-100"/>
                        <Badge onClick={() => {}} logo={lecture} title={lecture} hoverColor="bg-gray-100"/>
                        */}
                    </div>
                </div>
            }
            {dummy &&
                <div>
                    {/* Fake Loading Title */}
                    <div className="bg-gray-400 h-5 w-full rounded animate-pulse"></div>

                    <div className="flex flex-start">
                        {/* Fake Loading Owner */}
                        <div className="bg-gray-300 h-3 w-56 mt-2 rounded animate-pulse"></div>
                    </div>
                    {/*
                    <div className="flex flex-row mt-2 -ml-1">
                         Fake Loading Badges
                        <div className="bg-gray-200 h-5 w-36 rounded-full mr-2 ml-1 animate-pulse"></div>
                        <div className="bg-gray-200 h-5 w-28 rounded-full mr-2 ml-1 animate-pulse"></div>
                    </div>
                    */}
                </div>
            }
        </div>
    );
}

export default LectureListElement;
