import { useState, useEffect } from 'react';
import Button from '../components/Button';
import Guacamole from 'guacamole-common-js';
import config from '../config';
import { Modal } from 'rsuite';
import axios from 'axios';

const GuacamoleDisplay = ({ token, lectureID }) => {
    const [guacamoleClient, setGuacamoleClient] = useState(null);
    const [modalOpen, setModalOpen] = useState(false);

    useEffect(() => {
	console.log(lectureID);
        // Initialize guacamole client
        if (token) {
	    initGuacamole();
        }
    }, [token]);

    const handleOpen = () => setModalOpen(true);
    const handleClose = () => setModalOpen(false);

    const initGuacamole = () => {
        let gClient = new Guacamole.Client(new Guacamole.WebSocketTunnel(config.remote.serverurl));
        gClient.onerror = error => {
            console.log("ERROR Guacamole Client -> ", error);
        };
        setGuacamoleClient(gClient);
    }

    const connect = () => {
        guacamoleClient.connect(`token=${token}`);
        let display = document.getElementById("display");
        var mouse = new Guacamole.Mouse(display);
        mouse.onmousemove = (state) => guacamoleClient.sendMouseState(state)
        mouse.onmouseup = (state) => guacamoleClient.sendMouseState(state)
        mouse.onmousedown = (state) => guacamoleClient.sendMouseState(state)

        var keyboard = new Guacamole.Keyboard(document);
        keyboard.onkeydown = keysm => guacamoleClient.sendKeyEvent(1, keysm);
        keyboard.onkeyup = keysm => guacamoleClient.sendKeyEvent(0, keysm);

        display.appendChild(guacamoleClient.getDisplay().getElement())
	document.getElementById('display').firstChild.style.zIndex = "1";
    }

    const disconnect = () => {
        guacamoleClient.disconnect();
    }

    const rpcLogin = async () => {
       let url = ''
       if (config.api.hostname) url += config.api.hostname;
       if (config.api.port) url += `:${config.api.port}`;
       url += '/remotelogin/' + lectureID;
       console.log("Remote login initiated");
       let response = await axios.get(url);
    }

    const setSize = () => {
        guacamoleClient.sendSize(800, 600);
    }

    return (
        <div>
            <Button color="green" title="open" outline onClick={handleOpen}/>
	    <Modal full open={modalOpen} onClose={handleClose}>
	        <Modal.Header>
	            <Modal.Title>
	            <div className="flex flex-row">
                        {guacamoleClient &&
                            <Button color="green" title="Connect" outline onClick={event => connect()}/>
                        }
                        <Button color="blue" title="RPC-Login" outline onClick={rpcLogin}/>
                        <Button color="red" title="Disconnect" outline onClick={event => disconnect()}/>
	            </div>
	            </Modal.Title>
	        </Modal.Header>
                <Modal.Body>
	            <div id="display" className=""></div>
	        </Modal.Body>
            </Modal>
	</div>
    );
}

export default GuacamoleDisplay;
