import { useState } from 'react';
import { Table, Column, HeaderCell, Cell } from 'rsuite-table';
import { Checkbox, Panel } from 'rsuite';
import { BsTrash } from 'react-icons/bs';
import { ImDownload3 } from 'react-icons/im';
import Button from '../components/Button';
import UnixToDateCell from './table/UnixToDateCell';
import FormattedCell from './table/FormattedCell';
import CheckBoxCell from './table/CheckBoxCell';
import BoolIconCell from './table/BoolIconCell';

const HtmlIconCell = ({ rowData, onChange, checkedKeys, dataKey, ...props }) => (
    <Cell {...props} style={{ padding: 0 }}>
        <div style={{ lineHeight: '46px' }}>
            <div className="flex flex-row items-center"><div className="mr-2">{rowData[dataKey + '_html']}</div>{rowData[dataKey]}</div>
        </div>
    </Cell>
);

const getTotalSize = versions => {
    return "TODO";
    const fileSizes = versions.map(version => version.fileSize);
    const sum = fileSizes.reduce((partial_sum, element) => partial_sum + element, 0);
    return sum;
}

const TotalSizeCell = ({ rowData, onChange, checkedKeys, dataKey, ...props }) => (
    <Cell {...props} style={{ padding: 0 }}>
        <div style={{ lineHeight: '46px' }}>
            <div className="flex flex-row items-center justify-center">{getTotalSize(rowData)}</div>
        </div>
    </Cell>
);

const SelectableTable = ({ data }) => {
    const [checkedKeys, setCheckedKeys] = useState([]);
    let checked = false;
    let indeterminate = false;

    if (checkedKeys.length === data.length) {
        checked = true;
    } else if (checkedKeys.length === 0) {
        checked = false;
    } else if (checkedKeys.length > 0 && checkedKeys.length < data.length) {
        indeterminate = true;
    }

    const handleCheckAll = (value, checked) => {
        const keys = checked ? data.map(item => item.imageBaseId) : [];
        setCheckedKeys(keys);
    };
    const handleCheck = (value, checked) => {
        const keys = checked ? [...checkedKeys, value] : checkedKeys.filter(item => item !== value);
        setCheckedKeys(keys);
    };

    return (
        <div className="bg-gray-100 h-full p-10">
            <Panel header="Images" bordered bodyFill className="bg-white">
                <Table height={300} data={data} id="table">
                    <Column width={50} align="center" fixed>
                        <HeaderCell style={{ padding: 0 }}>
                            <div style={{ lineHeight: '40px' }}>
                                <Checkbox
                                    inline
                                    checked={checked}
                                    indeterminate={indeterminate}
                                    onChange={handleCheckAll}
                                />
                            </div>
                        </HeaderCell>
                        <CheckBoxCell dataKey="imageBaseId" checkedKeys={checkedKeys} onChange={handleCheck} />
                    </Column>

                    <Column width={350} sortable>
                        <HeaderCell>Name</HeaderCell>
                        <Cell dataKey="imageName"></Cell>
                    </Column>

                    <Column width={250} sortable>
                        <HeaderCell>OS</HeaderCell>
                        <HtmlIconCell dataKey="os"></HtmlIconCell>
                    </Column>

                    <Column width={250} sortable>
                        <HeaderCell>Owner</HeaderCell>
                        <Cell dataKey="owner"></Cell>
                    </Column>

                    <Column width={150} sortable>
                        <HeaderCell>Virtualizer</HeaderCell>
                        <HtmlIconCell dataKey="virtId"></HtmlIconCell>
                    </Column>

                    <Column width={150} sortable>
                        <HeaderCell>Last change</HeaderCell>
                        <UnixToDateCell dataKey="updateTime"></UnixToDateCell>
                    </Column>

                    <Column width={150} sortable>
                        <HeaderCell>Expiring</HeaderCell>
                        <UnixToDateCell dataKey="expireTime"></UnixToDateCell>
                    </Column>

                    <Column width={100} sortable>
                        <HeaderCell>Size</HeaderCell>
                        <FormattedCell dataKey="fileSize"></FormattedCell>
                    </Column>

                    <Column width={80} sortable>
                        <HeaderCell>Usable</HeaderCell>
                        <BoolIconCell dataKey="isRestricted"></BoolIconCell>
                    </Column>

                    <Column width={90} sortable>
                        <HeaderCell>Template</HeaderCell>
                        <BoolIconCell dataKey="isTemplate"></BoolIconCell>
                    </Column>

                    <Column width={100} sortable>
                        <HeaderCell># Versions</HeaderCell>
                        <BoolIconCell dataKey="versionCount"></BoolIconCell>
                    </Column>

                    <Column width={90} sortable>
                        <HeaderCell>Total size</HeaderCell>
                        <TotalSizeCell dataKey="versions"></TotalSizeCell>
                    </Column>

                </Table>

                <div className="flex flex-row-reverse m-4">
                    {/*
                    <ButtonToolbar>
                        <Button color="blue" appearance="default"><div className="flex flex-row items-center"><ImDownload3 className="mr-2"/>Download</div></Button>
                        <Button color="red" appearance="primary"><div className="flex flex-row items-center"><BsTrash className="mr-2"/>DELETE</div></Button>
                    </ButtonToolbar>
                    */}
                    <Button icon={ImDownload3} iconStroke="0" title="Downlaod" color="blue" outline/>
                    <Button icon={BsTrash} iconStroke="0" title={`Delete ${checkedKeys.length}`} color="red" outline disabled={checkedKeys.length <= 0}/>
                </div>
            </Panel>
        </div>
    );
}

export default SelectableTable;
