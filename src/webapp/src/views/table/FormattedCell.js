import { Cell } from 'rsuite-table';

const FormattedCell = ({ rowData, onChange, checkedKeys, dataKey, ...props }) => (
    <Cell {...props} style={{ padding: 0 }}>
        <div style={{ lineHeight: '46px' }}>
            <div className="flex flex-row items-center">{rowData[dataKey + '_formatted']}</div>
        </div>
    </Cell>
);

export default FormattedCell;
