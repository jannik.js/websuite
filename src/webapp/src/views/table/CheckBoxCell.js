import { useState } from 'react';
import { Cell, Column, } from 'rsuite-table';
import { Checkbox } from 'rsuite';

const CheckBoxColumn = ({ rowData, onChange, checkedKeys, dataKey, ...props }) => {
    return (
        <Cell {...props} style={{ padding: 0 }}>
            <div style={{ lineHeight: '46px' }}>
                <Checkbox
                    value={rowData[dataKey]}
                    inline
                    onChange={onChange}
                    checked={checkedKeys.some(item => item === rowData[dataKey])}
                    />
            </div>
        </Cell>
    );
}

export default CheckBoxColumn;
