import { Cell } from 'rsuite-table';

const unixToDate = unixTimeStamp => {
    const date = new Date(unixTimeStamp * 1000);
    const year = date.getFullYear();
    const month = ('0' + (date.getMonth() + 1)).slice(-2);
    const day = date.getDate();
    const hours = date.getHours();
    let minutes = ('0' + date.getMinutes()).slice(-2);

    const formattedDate = day + '.' + month + '.' + year + ' ' + hours + ':' + minutes;
    return formattedDate;
}

const UnixToDateCell = ({ rowData, onChange, checkedKeys, dataKey, ...props }) => (
    <Cell {...props} style={{ padding: 0 }}>
        <div style={{ lineHeight: '46px' }}>
            <div className="flex flex-row items-center">{unixToDate(rowData[dataKey])}</div>
        </div>
    </Cell>
);

export default UnixToDateCell;
