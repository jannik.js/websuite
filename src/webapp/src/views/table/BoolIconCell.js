import { Cell } from "rsuite-table";
import { AiOutlineCheck, AiOutlineClose } from "react-icons/ai";

const BoolIconCell = ({ rowData, onChange, checkedKeys, dataKey, ...props }) => (
    <Cell {...props} style={{ padding: 0 }}>
        <div className="flex flex-row items-center justify-center text-xl h-full">
            {rowData[dataKey] ? <AiOutlineCheck className="text-green-500"/> : <AiOutlineClose className="text-red-500"/>}
        </div>
    </Cell>
);

export default BoolIconCell;
