import { useState } from 'react';
import TabList from './TabList';
import Button from '../components/Button';
import { HiOutlineUserCircle } from 'react-icons/hi';
import { IoLogOutOutline } from 'react-icons/io5';
import Login from './Login';
import Divider from '../components/Divider';

const Dashboard = () => {
    const [activeView, setActiveView] = useState(<div></div>);
    const [darkMode, setDarkMode] = useState(JSON.parse(localStorage.getItem('darkMode')) || false);
    const [sidebar, setSidebar] = useState(false);

    // TODO: SET TO FALSE AND IMPLEMENT LOGIN
    const [loggedIn, setLoggedIn] = useState(true);

    const saveDarkMode = (value) => {
        setDarkMode(value);
        localStorage.setItem('darkMode', value);
    }

    const toggleSidebar = () => {
        setSidebar(!sidebar);
    }

    const closeSidebar = () => {
        setSidebar(false);
    }

    const setView = (view) => {
        setActiveView(view);
        setSidebar(false);
    }

    const login = (username, password) => {
        // TODO: Authentification
        setLoggedIn(true);
    }

    return (
        <div className={`flex flex-col w-screen h-screen ${darkMode && "dark" }`}>
            {!loggedIn
                ? <Login onLogin={login}/>
                : (
                    /* TopBar could be placed here */
                    <div className="flex flex-row w-screen h-screen overflow-hidden">

                        {/* Left Sidebar */}
                        <div className="flex flex-col h-screen bg-gray-50 w-96 flex-shrink-0 flex-gro">

                            {/* Logo */}
                            <div className="flex justify-center items-center w-full h-25 flex-shrink-0 select-none">
                                <a href="/" className="">
                                    <img className="h-16 lg:h-20 p-2 lg:p-4 hidden lg:block" src="/webSuite_logo_long_sm.png" alt="<Logo>"/>
                                    <img className="h-16 p-2 block lg:hidden" src="/webSuite_logo_512.png" alt="<Logo>"/>
                                </a>
                            </div>

                            {/* Tabs, List and Buttons */}
                            <div className="flex flex-col w-full h-full overflow-auto">
                                <TabList onChooseElement={view => setView(view)}/>
                            </div>

                            <Divider/>

                            {/* Account */}
                            <div className="w-full h-20 flex-shrink-0 flex flex-row justify-start items-center p-2">
                                <Button icon={IoLogOutOutline} color="red" title="Logout PH" outline onClick={() => { setLoggedIn(false); }}/>
                                {/* <Button icon={HiOutlineUserCircle} color="gray" round bigIcon/> */}
                            </div>

                        </div>

                        {/* Main Container */}
                        <div className="w-full h-full overflow-auto">
                            {activeView}
                        </div>
                    </div>
                )
            }
        </div>
    );
}

export default Dashboard;
