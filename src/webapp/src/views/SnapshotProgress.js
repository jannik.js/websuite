import { useState, useEffect } from 'react';
import { useParams } from 'react-router';
import Button from '../components/Button';
import config from '../config';
import axios from 'axios';
import Progressbar from '../components/Progressbar';

const SnapshotProgress = () => {
    const [timer, setTimer] = useState(60);
    const [timerCallback, setTimerCallback] = useState(() => x => console.log("BOOOM"));
    const [state, setState] = useState("TIMEOUT");
    const [progress, setProgress] = useState(null);
    const [percent, setPercent] = useState(null);

    let { id } = useParams();

    useEffect(() => {
	console.log(timer)
        let timeout;
	if (timer > 0) {
            timeout = setTimeout(() => setTimer(timer - 1), 1000)
	} else if (timerCallback) {
	    clearTimeout(timeout);
	    timerCallback();
	}

	// Cleanup timer between renders
	return () => clearTimeout(timeout);
    }, [timer]);

    const save = () => {
	console.log("Saving session ...");
	// Use session id, rest should be saved in the slx db anyways
	const saveUrl = config.api.hostname + ':' + config.api.port + '/edits/' + id + '/save'
	axios.get(saveUrl).then(res => {
		console.log(res);
	});

	/* Set timer to call progress every 5 seconds */
	setTimerCallback(() => x => {
	    const url = config.api.hostname + ':' + config.api.port + '/edits/' + id + '/progress'
	    axios.get(url).then(response => {
	        console.log(response);
		const percent = response.data.progress.percent.replace('%', '');
		console.log(percent);
		if (percent <= 100) setTimer(5);

		setState(response.data.state);
		setProgress(response.data.progress);
		setPercent(percent);
	    });
	});
	setTimer(5);
    }

    // Closes the tab (and firefox) without initiating the saving of the session
    const cancel = () => {
        window.close();
    }

    return (
        <div className="w-full h-full flex flex-col items-center">
            <span>
               Id: {id}
            </span>
            <span>
                Timer: {timer}
            </span>
	    <div>
                {state} | {JSON.stringify(progress)}
	    </div>
	    {progress &&
	        <Progressbar className="w-64" progress={percent}/>
	    }
	    <div className="flex flex-col mt-20">
                <Button className="" color="green" title="Save Lecture" outline onClick={save}/>
                <Button className="mt-5 p-0" color="red" title="Cancel Edit" outline onClick={cancel}/>
	    </div>
        </div>
    );
}

export default SnapshotProgress;
