import { useEffect, useState } from 'react';
import Button from '../components/Button';
import useRequestData from "../hooks/useRequestData";
import { HiDownload } from "react-icons/hi";
import { MdModeEdit, MdDelete } from "react-icons/md";
import { AiOutlineCheck, AiOutlineClose } from "react-icons/ai";
import dataRequest from '../utils/dataRequest';
import Panel from '../components/Panel';
import { Nav, Checkbox } from 'rsuite';
import { Table, Column, HeaderCell, Cell } from 'rsuite-table';
import UnixToDateCell from './table/UnixToDateCell';
import FileSizeCell from './table/FileSizeCell';
import CheckBoxCell from './table/CheckBoxCell';
import BoolIconCell from './table/BoolIconCell';

const DetailsVMs = ({ vmID }) => {
    const [{ data: image, isLoading: imageIsLoading }, { setId }] = useRequestData('images');
    const [owner, setOwner] = useState(null);
    const [activeTab, setActiveTab] = useState('overview');
    const [{ data: operatingsystems }] = useRequestData('operatingsystems', false);
    const [{ data: users }] = useRequestData('users', false);
    const [{ data: sharemodes }] = useRequestData('sharemodes', false);

    const [checkedKeys, setCheckedKeys] = useState([]);
    const [checked, setChecked] = useState(false);
    const [indeterminate, setIndeterminate] = useState(false);

    useEffect(() => {
        if (vmID) setId(vmID)
    }, [vmID, setId]);

    useEffect(() => {
        if (image.versions) {
            if (checkedKeys.length === image.versions.length) {
                setChecked(true);
            } else if (checkedKeys.length === 0) {
                setChecked(false);
            } else if (checkedKeys.length > 0 && checkedKeys.length < image.versions.length) {
                setIndeterminate(true);
            }
        }
    }, [checkedKeys]);

    // After image was loaded get the user from the owner id
    useEffect(() => {
        const getUser = async ownerId => {
            if (!ownerId) return '';
            const users = await dataRequest('users');
            let user = users.find(u => u.userId === ownerId);
            setOwner(user);
        }
        getUser(image.ownerId);
    }, [imageIsLoading])

    const CustomNav = ({ active, onSelect, ...props }) => {
        return (
          <Nav {...props} activeKey={active} onSelect={onSelect} className="bg-gray-100 flex flex-wrap">
            <Nav.Item eventKey="overview">Overview</Nav.Item>
            <Nav.Item eventKey="vmversions">VM Versions</Nav.Item>
            <Nav.Item eventKey="permissions">Permissions</Nav.Item>
          </Nav>
        );
      };

    const unixToDate = unixTimeStamp => {
        const date = new Date(unixTimeStamp * 1000);
        const year = date.getFullYear();
        const month = ('0' + (date.getMonth() + 1)).slice(-2);
        const day = date.getDate();
        const hours = date.getHours();
        let minutes = ('0' + date.getMinutes()).slice(-2);

        const formattedDate = day + '.' + month + '.' + year + ' ' + hours + ':' + minutes;
        return formattedDate;
    }

    const getOsFromId = id => {
        const os = operatingsystems.find(os => os.osId === image.osId);
        return os ? os.osName : undefined;
    }

    const getUserFromId = id => {
        const user = users.find(user => user.userId === id);
        return user ? user.firstName + ' ' + user.lastName : undefined;
    }

    const UserIdToNameCell = ({ rowData, dataKey, ...props }) => (
        <Cell {...props} style={{ padding: 0 }}>
            <div style={{ lineHeight: '46px' }}>
                <div className="flex flex-row items-center"><div className="mr-2">{rowData[dataKey + '_html']}</div>{getUserFromId(rowData[dataKey])}</div>
            </div>
        </Cell>
    );

    /*
    const [checkedKeys, setCheckedKeys] = useState([]);
    let checked = false;
    let indeterminate = false;

    console.log(image.versions)
    if (image.versions && checkedKeys.length === image.versions.length) {
        checked = true;
    } else if (checkedKeys.length === 0) {
        checked = false;
    } else if (image.versions && checkedKeys.length > 0 && checkedKeys.length < image.versions.length) {
        indeterminate = true;
    }
    */
    const handleCheckAll = (value, checked) => {
        const keys = checked ? image.versions.map(item => item.versionId) : [];
        setCheckedKeys(keys);
    };
    const handleCheck = (value, checked) => {
        const keys = checked ? [...checkedKeys, value] : checkedKeys.filter(item => item !== value);
        setCheckedKeys(keys);
    };


    return (
        <div className="min-h-full bg-gray-100">
            {/* Top Area */}
            <div className="flex flex-col bg-gray-100 text-gray-600 w-full pl-4 pt-2 pr-4 pb-2">

                <div className="flex flex-row-reverse flex-wrap">
                    {/* VM IDs */}
                    <div className="flex flex-col items-end flex-shrink-0 text-xs font-mono	ml-4 -mt-1 -mr-2">
                        <span><span className="select-none">Latest Version ID: </span>{image.latestVersionId}</span>
                        <span><span className="select-none">Image Base ID: </span>{image.imageBaseId}</span>
                    </div>
                </div>

                <div className="flex flex-grow items-center justify-between flex-wrap">
                    <div className="flex flex-col xl:flex-row">
                        {/* VM Name */}
                        <h1 className="text-gray-600 font-bold text-3xl">{image.imageName}</h1>

                        {/* Owner */}
                        {owner &&
                            <h1 className="font-bold text-gray-500 self-end text-xl ml-4">by {owner.firstName} {owner.lastName}</h1>
                        }

                        {/* Dummy Loading for owner */}
                        {!owner &&
                            <div className="flex">
                                <div className="bg-gray-500 h-5 w-16 rounded animate-pulse"></div>
                                <div className="bg-gray-500 h-5 w-32 rounded animate-pulse ml-2"></div>
                            </div>
                        }
                    </div>

                    {/* Action Buttons */}
                    {!imageIsLoading &&
                        <div className="flex self-end">
                            <a href={`https://webapi.schoenartz.com/images/${image.imageBaseId}/versions/${image.latestVersionId}`}>
                                <Button title="Download" color="blue" icon={HiDownload} outline medium smallBorder disabled={!image.userPermissions.download}/>
                            </a>
                            <Button title="Edit" color="orange" icon={MdModeEdit} outline medium smallBorder disabled={!image.userPermissions.edit}/>
                            <Button title="Delete" color="red" icon={MdDelete} outline medium smallBorder disabled={!image.userPermissions.admin}/>
                        </div>
                    }

                    {/* Dummy Loading Buttons */}
                    {imageIsLoading &&
                        <div className="flex self-end">
                             <div className="bg-gray-500 h-5 w-32 rounded animate-pulse ml-2"></div>
                         </div>
                    }

                </div>
            </div>

            <div className="w-full h-0.5 bg-gray-300"></div>

            {/* Top menu bar */}
            <CustomNav appearance="subtle" active={activeTab} onSelect={setActiveTab} />

            {/* Main Area */}
            <div className="break-all">
                {!imageIsLoading &&
                    <div className="p-4">
                        {/* Overview */}
                        {activeTab === 'overview' &&
                            <div className="flex flex-wrap">

                                {/* General information */}
                                <Panel className="w-auto flex-none h-auto bg-white" title="Information">
                                    <div className="grid grid-cols-4 gap-x-4 gap-y-4">

                                        {/* Owner */}
                                        <span className="text-gray-500">Owner</span>
                                        {owner &&
                                             <span className="text-gray-600 font-bold text-sm col-span-3">{owner.firstName} {owner.lastName}</span>
                                        }
                                        {!owner &&
                                            <div className="flex mb-2 col-span-3">
                                                <div className="bg-gray-500 h-4 w-10 rounded animate-pulse"></div>
                                                <div className="bg-gray-500 h-4 w-16 rounded animate-pulse ml-1"></div>
                                            </div>
                                        }

                                        {/* Creation time */}
                                        <span className="text-gray-500">Created on</span>
                                        <span className="text-gray-600 font-bold text-sm col-span-3">{unixToDate(image.createTime)}</span>

                                        {/* Changed by */}
                                        <span className="text-gray-500">Changed by</span>
                                        <span className="text-gray-600 font-bold text-sm col-span-3">{getUserFromId(image.updaterId)}</span>

                                        {/* Changed on */}
                                        <span className="text-gray-500">Changed on</span>
                                        <span className="text-gray-600 font-bold text-sm col-span-3">{unixToDate(image.updateTime)}</span>

                                        {/* Operating system */}
                                        <span className="text-gray-500">Operating System</span>
                                        <span className="text-gray-600 font-bold text-sm col-span-3">{getOsFromId(image.osId)}</span>

                                        {/* Share mode */}
                                        <span className="text-gray-500">Share Mode</span>
                                        <span className="text-gray-600 font-bold text-sm col-span-3">{sharemodes[image.shareMode]} {image.isTemplate && "(Template)"}</span>

                                        {/* Version ID */}
                                        <span className="text-gray-500">Version ID</span>
                                        <span className="text-gray-600 font-bold text-sm col-span-3">{image.latestVersionId}</span>

                                        {/* VM ID */}
                                        <span className="text-gray-500">VM ID</span>
                                        <span className="text-gray-600 font-bold text-sm col-span-3">{image.imageBaseId}</span>

                                        {/* Virtualizer */}
                                        <span className="text-gray-500">Virtualizer</span>
                                        <span className="text-gray-600 font-bold text-sm col-span-3">{image.virtId}</span>

                                        {/* Lectures */}
                                        <span className="text-gray-500">Lectures</span>
                                        <span className="text-gray-600 font-bold text-sm col-span-3">{}</span>

                                        {/* Versions */}
                                        <span className="text-gray-500">Versions</span>
                                        <span className="text-gray-600 font-bold text-sm col-span-3">{image.versions.length}</span>

                                    </div>
                                </Panel>

                                {/* Description */}
                                <Panel className="flex-grow h-auto bg-white" title="Description">
                                    <pre>
                                        {image.description}
                                    </pre>
                                </Panel>
                            </div>
                        }

                        {/* Vm Versions */}
                        {activeTab === 'vmversions' &&
                            <div className="flex flex-wrap">

                                <Panel className="bg-white w-full h-auto" title="Versions">
                                    <Table height={300} data={image.versions} id="table">

                                        <Column width={50} sortable>
                                            <HeaderCell style={{ padding: 0 }}>
                                                <div style={{ lineHeight: '40px' }}>
                                                    <Checkbox
                                                        inline
                                                        checked={checked}
                                                        indeterminate={indeterminate}
                                                        onChange={handleCheckAll}
                                                    />
                                                </div>
                                            </HeaderCell>
                                            <CheckBoxCell dataKey="versionId" checkedKeys={checkedKeys} onChange={handleCheck} />
                                        </Column>

                                        <Column width={350} sortable>
                                            <HeaderCell>ID</HeaderCell>
                                            <Cell dataKey="versionId"></Cell>
                                        </Column>

                                        <Column width={250} sortable>
                                            <HeaderCell>Uploader</HeaderCell>
                                            <UserIdToNameCell dataKey="uploaderId"></UserIdToNameCell>
                                        </Column>

                                        <Column width={150} sortable>
                                            <HeaderCell>Creation</HeaderCell>
                                            <UnixToDateCell dataKey="createTime"></UnixToDateCell>
                                        </Column>

                                        <Column width={150} sortable>
                                            <HeaderCell>Expiring</HeaderCell>
                                            <UnixToDateCell dataKey="expireTime"></UnixToDateCell>
                                        </Column>

                                        <Column width={100} sortable>
                                            <HeaderCell>Size</HeaderCell>
                                            <FileSizeCell dataKey="fileSize"></FileSizeCell>
                                        </Column>

                                        <Column width={100} sortable>
                                            <HeaderCell>Restricted</HeaderCell>
                                            <BoolIconCell dataKey="isRestricted"></BoolIconCell>
                                        </Column>

                                        <Column width={80} sortable>
                                            <HeaderCell>Valid</HeaderCell>
                                            <BoolIconCell dataKey="isValid"></BoolIconCell>
                                        </Column>

                                        <Column width={100} sortable>
                                            <HeaderCell>Processed</HeaderCell>
                                            <BoolIconCell dataKey="isProcessed"></BoolIconCell>
                                        </Column>
                                    </Table>

                                    <div className="flex flex-row-reverse m-4">
                                        <Button iconStroke="0" title="Downlaod" color="blue" outline disabled/>
                                        <Button iconStroke="0" title={`Delete`} color="red" outline disabled/>
                                    </div>
                                </Panel>
                            </div>
                        }

                        {/* Permissions */}
                        {activeTab === 'permissions' &&
                            <div className="flex flex-wrap">
                                <Panel className="w-auto h-auto bg-white" title="Permissions">
                                    <div className="grid grid-cols-5 gap-x-4 gap-y-4">

                                        {/* Permission header */}
                                        <div className="font-bold"></div>
                                        <div className="font-bold flex justify-center">Link</div>
                                        <div className="font-bold flex justify-center">Download</div>
                                        <div className="font-bold flex justify-center">Edit</div>
                                        <div className="font-bold flex justify-center">Admin</div>

                                        {/* Default permissions */}
                                        <div className="font-bold">Default</div>
                                        <div className="flex justify-center text-xl">{image.defaultPermissions['link'] ? <AiOutlineCheck className="text-green-500"/> : <AiOutlineClose className="text-red-500"/>}</div>
                                        <div className="flex justify-center text-xl">{image.defaultPermissions['download'] ? <AiOutlineCheck className="text-green-500"/> : <AiOutlineClose className="text-red-500"/>}</div>
                                        <div className="flex justify-center text-xl">{image.defaultPermissions['edit'] ? <AiOutlineCheck className="text-green-500"/> : <AiOutlineClose className="text-red-500"/>}</div>
                                        <div className="flex justify-center text-xl">{image.defaultPermissions['admin'] ? <AiOutlineCheck className="text-green-500"/> : <AiOutlineClose className="text-red-500"/>}</div>

                                        {/* User permissions */}
                                        <div className="font-bold">User</div>
                                        <div className="flex justify-center text-xl">{image.userPermissions['link'] ? <AiOutlineCheck className="text-green-500"/> : <AiOutlineClose className="text-red-500"/>}</div>
                                        <div className="flex justify-center text-xl">{image.userPermissions['download'] ? <AiOutlineCheck className="text-green-500"/> : <AiOutlineClose className="text-red-500"/>}</div>
                                        <div className="flex justify-center text-xl">{image.userPermissions['edit'] ? <AiOutlineCheck className="text-green-500"/> : <AiOutlineClose className="text-red-500"/>}</div>
                                        <div className="flex justify-center text-xl">{image.userPermissions['admin'] ? <AiOutlineCheck className="text-green-500"/> : <AiOutlineClose className="text-red-500"/>}</div>

                                    </div>
                                </Panel>
                            </div>
                        }

                    </div>
                }
            </div>
        </div>
    );
}

export default DetailsVMs;
