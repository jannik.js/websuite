import { useState } from 'react';
import Button from '../components/Button';
import { IoLogInOutline } from 'react-icons/io5';
import { FaUser, FaKey, FaEye, FaEyeSlash } from 'react-icons/fa';
import { Input, InputGroup } from 'rsuite';

const Login = ({ onLogin = () => {} }) => {
    const [visible, setVisible] = useState(false);

    return (
        <div className="w-full h-full flex flex-col items-center justify-center bg-gray-100">

            <div className="flex flex-col w-96 p-8 bg-gray-100 border-solid border-gray-300 border">
                {/* Logo */}
                <div className="flex justify-center items-center w-full mt-8 mb-8 flex-shrink-0 select-none">
                    <img className="h-32 p-4" src="/webSuite_logo_long_sm.png" alt="<Logo>"/>
                </div>

                <div className="flex flex-col justify-center">
                    {/* Login inputs */}
                    <div className="flex flex-col">
                        <InputGroup className="mb-4">
                            <InputGroup.Addon>
                                <FaUser/>
                            </InputGroup.Addon>
                            <Input placeholder="Username"/>
                        </InputGroup>
                        <InputGroup>
                            <InputGroup.Addon>
                                <FaKey/>
                            </InputGroup.Addon>

                            <Input type={visible ? 'text' : 'password'} placeholder="Password"/>
                            <InputGroup.Button onClick={() => { setVisible(!visible) }} style={{"background-color": "transparent"}}>
                                {!visible ? <FaEye/> : <FaEyeSlash/>}
                            </InputGroup.Button>
                        </InputGroup>
                    </div>

                    {/* Buttons */}
                    <div className="flex flex-row-reverse w-full mt-8 ">
                        <Button icon={IoLogInOutline} color="cyan" title="Login PH" outline onClick={onLogin}/>
                    </div>
                </div>

            </div>

        </div>
    );
}

export default Login;
