import { useState, useMemo } from 'react';

const useSortableData = ({ items, config = null }) => {
    const [sortConfig, setSortConfig] = useState(config);

    const sortedItems = useMemo(() => {
        let sortableItems = [...items];
        if (sortConfig !== null) {
            sortableItems.sort((a, b) => {

                // Item is smaller
                if (a[sortConfig.key] < b[sortConfig.key]) {
                    return sortConfig.direction === 'ascending' ? -1 : 1;
                }

                // Item is bigger
                if (a[sortConfig.key] > b[sortConfig.key]) {
                    return sortConfig.direction === 'ascending' ? 1 : -1;
                }

                // Item is equal
                return 0;
            });
        }

        return sortableItems;
    }, [items, sortConfig]);

    // This function handles the asc/desc sorting order on second click
    const requestSort = key => {
        let direction = 'ascending';

        if (sortConfig && sortConfig.key === key && sortConfig.direction === 'ascending') {
            direction = 'descending';
        }

        setSortConfig({ key, direction });
    }

    return { items: sortedItems, requestSort, sortConfig };
}

export default useSortableData;
