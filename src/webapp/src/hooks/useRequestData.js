import { useState, useEffect, useRef } from 'react';
import dataRequest from '../utils/dataRequest';

const useRequestData = (endpoint, skipFetching = true) => {
    const [data, setData] = useState([]);
    const [ressource, setRessource] = useState(endpoint);
    const [id, setId] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [isError, setIsError] = useState(false);
    const [fetchToggle, setFetchToggle] = useState(false);

    const skipFetch = useRef(skipFetching);

    useEffect(() => {
        // Function for calling the api and getting the data
        const fetch = async () => {
            if (!ressource) return;

            // Skip first initial fetching if it isn't set otherwise.
            if (skipFetch.current) return skipFetch.current = false;

            setIsLoading(true);
            setIsError(false);

            // Request the data from the api
            const response = await dataRequest(ressource, id);
            if (response.error) return setIsError(true);
            setData(response);
            setIsLoading(false)
        }

        fetch();
    }, [id, ressource, fetchToggle]);

    const fetchData = () => {
        setFetchToggle(!fetchToggle);
    }

    return [{ data, isLoading, isError }, { setRessource, setId, fetchData }];
}

export default useRequestData;
