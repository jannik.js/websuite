function Button({ children, title, icon, iconStroke = 1, iconColor, color, className = "", onClick = event => {}, full, round, outline, bigIcon, small, medium, smallBorder, disabled, bgColored }) {
    const Icon = icon;
    const buttonColor = disabled ? 'gray' : color;

    return (
        <div className={`${full && "w-full"} rounded-full ml-1 mr-1`}> {/* p-0.5 bg-gradient-to-r from-${color}-400 to-blue-500 hover:from-yellow-500 hover:to-red-500 */}
            <button
                className={`
                    rounded-full focus:outline-none w-full font-bold transition-colors duration-300
                    ${outline && (smallBorder ? "border" : "border-2")}
                    text-${buttonColor}-400 border-${buttonColor}-400
                    ${!disabled && (bgColored ? `bg-${buttonColor}-100` : `bg-gray-50`)}
                    ${!disabled && `hover:bg-gray-50 hover:border-${buttonColor}-500 hover:text-${buttonColor}-500`}
                    ${!disabled && `active:bg-${buttonColor}-400 active:text-white`}
                    ${disabled && 'cursor-default'}
                    inline-flex items-center ${round ? "p-0" : (small ? "py-0.5 px-2 text-xs" : (medium ? "py-0.5 px-3 text-sm" : "py-1 px-6"))} ${className}
                `}
                onClick={event => onClick(event)}
                disabled={disabled}
            >
                <div className={`flex justify-center items-center w-full`}>
                    {Icon &&
                        <span className={`${bigIcon ? "text-4xl" : (small ? "text-sm" : (medium ? "text-lg" : "text-2xl"))}`}>
                            {<Icon className={`stroke-${iconStroke} text-${iconColor}-400`}/>}
                        </span>
                    }
                    {title &&
                        <span className={`${icon && "ml-2"}`}>
                            {title && title.toUpperCase()}
                        </span>
                    }
                </div>
                {children}
            </button>
        </div>
    );
}

export default Button;
