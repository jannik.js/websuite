import React from 'react';

const Panel = ({ title, value, className = "", children, color = "white" }) => {
    return (
        <div className={`w-72 rounded-lg m-2 flex bg-${color} ${className}`}>
            <div className="flex flex-col w-full h-full">

                <div className="text-xl font-bold m-2 p-2 mb-0 bg-gray-50 rounded-t-lg border-b-2 border-solid border-gray-200">
                    {title}
                </div>

                <div className="m-4 mt-2 pt-2 h-full">
                    {children}
                </div>

            </div>
        </div>
    );
}

export default Panel;
