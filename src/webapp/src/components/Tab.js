const Tab = ({ icon, title, selected, id, onClick = () => {} }) => {
    return (
        <div className={`flex flex-row pt-2 pb-2 pl-8 pr-8 items-center justify-center m-4 select-none
                ${selected === id ? "bg-gray-100 text-lightBlue-400 hover:text-lightBlue-500 shadow" : "text-gray-500"}
                hover:bg-gray-100 rounded-lg active:text-gray-700
                transition-all
                lg:flex-col
            `}
            onClick={() => onClick(id)}
        >
            {/* Icon */}
            <div className="pr-2 text-xl lg:p-0 md:text-2xl lg:text-2xl 2xl:text-4xl">
                {icon}
            </div>

            {/* Text */}
            <div className="font-semibold">
                {title}
            </div>
        </div>
    );
}

export default Tab;
