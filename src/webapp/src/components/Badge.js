import { IoCloseSharp } from "react-icons/io5";

const Badge = ({ children, logo, title, onClick = () => {}, onCancel, hoverColor = "bg-gray-300" }) => {
    const cancel = event => {
        event.stopPropagation();
        onCancel();
    }

    const click = event => {
        event.stopPropagation();
        onClick();
    }

    return (
        <span
            className={`
                flex items-center justify-center
                m-1 px-2 py-1 rounded-full cursor-pointer
                bg-gray-200 text-gray-600 font-semibold text-xs
                hover:${hoverColor}
            `}
            onClick={event => click(event)}
        >
            {logo &&
                <span className="mr-1 inline-block">
                    {logo}
                </span>
            }
            {title}
            {children}
            {onCancel &&
                <span className="ml-2 hover:text-red-600" onClick={event => cancel(event)}>
                    <IoCloseSharp/>
                </span>
            }
        </span>
    );
}

export default Badge;
