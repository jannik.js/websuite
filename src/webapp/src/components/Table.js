import { useState } from 'react';
import useSortableData from '../hooks/useSortableData';
import { IoCaretUpOutline, IoCaretDownOutline } from "react-icons/io5";

const Table = ({ headers, data, className, onClick = () => {}, onClickReturnKey, loading, multiselect = false }) => {
    const { items, requestSort, sortConfig } = useSortableData({ items: data });
    const [selectedItems, setSelectedItems] = useState({});

    const getSortDirection = (header) => {
        if (sortConfig && sortConfig.key === header) {
            return sortConfig.direction === 'ascending' ? <IoCaretUpOutline/> : <IoCaretDownOutline/> ;
        }
    }

    return (
        <div className={`bg-gray-300 dark:bg-gray-800 shadow-md rounded ${className}`}>
            <table className="w-full mb-2">

                {/* Headers */}
                <thead className="text-gray-600 dark:text-gray-300 w-full table table-fixed">
                    <tr className="uppercase text-sm leading-normal hidden md:table-row">
                        {headers.map((header, index) => (
                            <th key={index} className="px-6 py-3 text-left cursor-pointer" onClick={() => requestSort(header)}>
                                <div className="inline-flex items-center">{header} {getSortDirection(header)}</div>
                            </th>
                        ))}
                    </tr>
                </thead>

                <tbody className="divide-y-8 divide-white md:divide-y-0 text-gray-600 dark:text-gray-300 dark:bg-gray-700 text-sm font-light overflow-y-scroll block h-screen-3/4 w-full">
                    {/* Placeholder data "loading"-animation */}
                    {loading && [...Array(50)].map((element, index) => (
                        <tr key={index} className={`border-b border-gray-200 dark:border-gray-900 grid pt-2 pb-2 md:table w-full table-fixed ${index % 2 !== 0 ? "bg-gray-100 dark:bg-gray-600" : "bg-gray-200 dark:bg-gray-700"}`}>
                            {headers.map((header, index2) => (
                                <td key={index2} className={`grid grid-cols-4 md:table-cell py-1 px-3 text-left break-all`}>
                                    <div className="inline md:hidden font-medium">{header}: </div>
                                    <div className="bg-gray-300 dark:bg-gray-800 h-5 w-full rounded animate-pulse ml-2 md:ml-0 col-span-3"></div>
                                </td>
                            ))}
                        </tr>
                    ))}

                    {/* The actual data */}
                    {!loading && items.map((row, index) => (
                        <tr
                            key={index}
                            onClick={() => {
                                if (multiselect) {
                                    const value = !selectedItems[index];
                                    let items = selectedItems;
                                    if (value) items[index] = value;
                                    else delete items[index];
                                    setSelectedItems(items);
                                } else onClick(row[onClickReturnKey]);
                            }}
                            className={
                                `border-l-4 border-r-4 border-white hover:bg-gray-300 grid divide-y-2 divide-gray-200 pt-2 pb-2 w-full table-fixed cursor-pointer
                                md:border-0 md:border-gray-200 md:table md:divide-y-0 dark:border-gray-900 dark:hover:bg-gray-500
                                ${index % 2 !== 0 ? "bg-gray-100 dark:bg-gray-600" : "bg-gray-100 md:bg-gray-200 dark:bg-gray-700"}
                                ${selectedItems[index] && "md:bg-blue-200 md:hover:bg-blue-300"}`
                            }
                        >

                            {headers.map((header, index2) => (
                                <td key={index2}
                                    className={
                                        `grid grid-cols-4 gap-x-4 md:table-cell py-0 md:py-1 px-3 text-left break-all
                                        ${index2 === 0 && "font-medium"}
                                        ${(row.classNames && row.classNames[header]) ? row.classNames[header] : "" }`
                                    }
                                >
                                    <div className="inline md:hidden font-medium">{header}: </div><div className="inline col-span-3">{row[header]}</div>
                                </td>
                            ))}

                        </tr>
                    ))}
                </tbody>

            </table>
        </div>
    );
}

export default Table;
