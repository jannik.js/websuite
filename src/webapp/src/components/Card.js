const Card = ({ title, value, image, icon, footerValue }) => {
    const Icon = icon;
    return (
        <div className="w-72 h-32 rounded-lg m-8 flex bg-white p-4">
            <div className="flex items-center">
                {image &&
                    <img className="object-scale-down w-28" src={image} alt={title}/>
                }
                {icon &&
                    <Icon className="h-24 w-24 text-gray-700"/>
                }
            </div>
            <div className="flex flex-col divide-y-2 pl-4 w-full">
                <div className="text-xl font-bold">
                {title}
                </div>
                <div className="mt-2 pt-2">
                    <div className="flex flex-row">
                        <div className="flex flex-col">
                            <div>
                                {value}
                            </div>
                            <div className="text-sm">
                                {footerValue}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Card;
