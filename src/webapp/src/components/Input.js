import React, { useState } from "react";

const Input = ({ children, type, icon, name, placeholder, onChange, inputAction, suggestionData, value }) => {
    const [inputValue, setInputValue] = useState(value);
    const [showSuggestions, setShowSuggestions] = useState(false);
    const [selectedSuggestion, setSelectedSuggestion] = useState(0);
    const [suggestions, setSuggestions] = useState(suggestionData);

    const onInputChange = event => {
        setInputValue(event.target.value);

        // Refilter suggestionData
        if (event.target.value.length > 0) {
            setShowSuggestions(true);
            setSelectedSuggestion(0);
            // TODO maybe use Levenshtein or hemming distance for better results
            setSuggestions([event.target.value, ...suggestionData.filter(data => {
                const included = data.toLowerCase().includes(event.target.value.toLowerCase());
                const notSame = data.toLowerCase() !== event.target.value.toLowerCase() ? true : false;
                return included && notSame;
            })]);

        } else setShowSuggestions(false);
        onChange(event);
    }

    const onInputKeyPressed = event => {
        switch (event.key) {
            case 'Enter':
                inputAction(event.target.value);
                setShowSuggestions(false);
                setInputValue('');
            break;

            // Arrow down
            case 'ArrowDown':
                event.preventDefault();
                if (suggestions.length > selectedSuggestion + 1) {
                    setSelectedSuggestion(selectedSuggestion + 1);
                    setInputValue(suggestions[selectedSuggestion + 1]);
                }
                break;

            //Arrow up
            case 'ArrowUp':
                event.preventDefault();
                if (selectedSuggestion - 1 >= 0) {
                    setSelectedSuggestion(selectedSuggestion - 1);
                    setInputValue(suggestions[selectedSuggestion - 1]);
                }
                break;

            case 'Escape':
                setShowSuggestions(false);
                setInputValue('');
                break;

            default:
        }
    }

    const clickSuggestion = data => {
        inputAction(data);
        setShowSuggestions(false);
        setInputValue('');
    }

    return (
        <div className="relative rounded-full shadow-sm">
            <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                <span className="text-gray-500 sm:text-sm">{icon}</span>
            </div>
            <input
                type={type}
                name={name}
                id={name}
                className={`border block w-full pl-7 pr-12 sm:text-sm border-gray-200 ${showSuggestions ? "rounded-t-2xl rounded-b-none border-b" : "rounded-full" /* focus:border-b-2 */}
                focus:ring-0 focus:border-gray-300 hover:shadow-md focus:shadow-md`} /* focus:border-l-2 focus:border-r-2 focus:border-t-2 */
                value={inputValue}
                placeholder={placeholder}
                onChange={event => onInputChange(event)}
                onKeyDown={event => onInputKeyPressed(event)}
                onFocus={event => {
                    if (event.target.value.length > 0) setShowSuggestions(true)
                }}
                onBlur={() => setShowSuggestions(false)}
            />
            {suggestionData && showSuggestions && (
                <ul className="absolute bg-white w-full max-h-64 overflow-auto rounded-b-2xl border-l border-b border-r border-gray-300 shadow-md"> {/* focus:border-l-2 focus:border-r-2 focus:border-t-2 */}
                    {suggestions.map((data, index) => (
                        <li
                            key={index}
                            className={`p-2 hover:bg-gray-200 cursor-pointer ${selectedSuggestion === index ? "bg-gray-200" : "bg-white"}`}
                            onMouseDown={() => clickSuggestion(data)}
                            onMouseOver={() => setSelectedSuggestion(index)}
                        >
                            {data}
                        </li>
                    ))}
                </ul>
            )}
        </div>
    )
}

export default Input;
