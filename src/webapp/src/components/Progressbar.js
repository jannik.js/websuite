import React from 'react';

const Progressbar = ({ progress = 0, paused = false, animated = true, error = false, className = "" }) => {
    let color = paused ? 'yellow' : 'green';
    color = error ? 'red' : color;
    return (
        <div className={`bg-gray-200 rounded h-6 mt-5 ${className}`}>
            <div className={`bg-${color}-400 rounded h-6 text-center text-white text-sm transition`} style={{ width: progress + "%" }}>
                {progress}%
            </div>
        </div>
    );
}

export default Progressbar;
