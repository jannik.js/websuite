import React from 'react';

const Divider = ({ className }) => {
    return (
        <div className={`w-full bg-gray-200 h-0.5 mt-0.5 mb-0.5${className}`}/>
    );
}

export default Divider;
