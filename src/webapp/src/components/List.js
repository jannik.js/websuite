import Badge from './Badge';
import { useState, useEffect } from 'react';
import { IoSearchSharp } from "react-icons/io5";
import Input from "./Input";

/*
 * listElement: Has to be a view that displays the object data as element. (VmListElement, LectureListElement, ...)
 * listOfFilterValues: A list of the values in the objects that can be used as filter.
 */
const List = ({ data, onClick = () => {}, onClickReturnKey, className = "", filter, loading, listElement, listOfFilterValues }) => {
    const [listOfFilters, setListOfFilters] = useState([]);
    const [filteredData, setFilteredData] = useState(data);
    const ListElement = listElement;

    // Creates a list of all values that are existent in data. Only consideres the values of listOfFilterValues (all key if the list wasn't given).
    const getFilters = () => {
        let filterList = {};
        for (let obj of data) {
            let keys = [];
            if (listOfFilterValues.length > 0) keys = listOfFilterValues;
            else keys = Object.keys(obj);

            for (let key of keys) {
                if (!(key in filterList)) filterList[key] = [];
                if (!filterList[key].some(object => object.value === obj[key])) {
                    // If an e.g. an icon should be at the beginning of the filtername, one can define <key>_html in the object with the html of the icon.
                    filterList[key].push({
                        value: obj[key],
                        type: key,
                        html: (key + '_html' in obj) ? obj[key + '_html'] : undefined,
                        active: false,
                    });
                }
            }
        }

        let result = [];
        const keys = Object.keys(filterList);
        for (let key of keys) {
            result = [...result, ...filterList[key]];
        }

        return result;
    }

    const changeFilter = (filterName, activeValue) => {
        let filterExists = false;
        let newFilters = listOfFilters.map(filter => {
            if (filter.value === filterName) {
                filterExists = true;
                return { ...filter, active: activeValue };
            } else return filter;
        });

        // If filter exists everything is fine. Set new filters and exit function.
        if (filterExists) {
            setListOfFilters(newFilters);
            return;
        }

        // If no filter exists it should be handled as a search term.
        // TODO
    }

    useEffect(() => {
        const filters = getFilters();
        setListOfFilters(filters);
    }, [data]);

    useEffect(() => {
        // Get all active filters
        let activeFilters = listOfFilters.filter(filter => filter.active);//;

        // Filter the data. If same type use OR between different types use AND
        if (activeFilters.length > 0) {
            // Create a dictionary with types as keys and lists of the filters as value.
            let filterLists = {};
            for (let filter of activeFilters) {
                if (!(filter.type in filterLists)) filterLists[filter.type] = [];
                filterLists[filter.type].push(filter.value);
            }

            // Filter the data by the different type lists
            const filterTypes = Object.keys(filterLists);
            let newData = data.filter(element => {
                for (let type of filterTypes) {
                    if (!filterLists[type].includes(element[type])) return false;
                }
                return true;
            });
            setFilteredData(newData);
        } else setFilteredData(data);
    }, [listOfFilters]);

    return (
        <div className={`${className} overflow-auto flex flex-col h-full`}>

            {/* Filter */}
            {!loading && filter &&
                <div className="flex flex-col bg-gray-50 p-2">
                    <Input type="text" icon={<IoSearchSharp/>} placeholder="Search" onChange={event => {}} inputAction={value => changeFilter(value, true)} suggestionData={listOfFilters.filter(filter => filter.active === false).map(filter => filter.value)}/>
                    {listOfFilters.filter(obj => obj.active === true).length > 0 && <div className="flex flex-row flex-wrap pt-2">
                        {listOfFilters.filter(obj => obj.active === true).map((filter, index) => (
                            <Badge key={index} onCancel={() => changeFilter(filter.value, false)} logo={filter.html} title={filter.value}/>
                        ))}
                    </div>}
                </div>
            }

            {/* List */}
            {!loading &&
                <div className="overflow-auto">
                    {filteredData.map((object, index) => (
                        <div
                            key={index}
                            /*className={`flex flex-col p-2 ${index % 2 !== 0 ? "bg-gray-50" : "bg-gray-100"} hover:bg-gray-300 cursor-pointer overflow-hidden`}*/
                            className={`flex flex-col p-2 ${index % 2 !== 0 ? "bg-gray-50" : "bg-white"} hover:bg-gray-200 cursor-pointer overflow-hidden`}
                            onClick={() => onClick(object[onClickReturnKey])}
                        >
                            <ListElement data={object} changeFilter={changeFilter}/>
                        </div>
                    ))}
                </div>
            }

            {/* List Dummy for the "loading"-animation */}
            {loading &&
                <div className="flex flex-col bg-gray-100">
                    <div className="bg-white border-gray-200 border-2 h-10 w-92 m-2 rounded-full animate-pulse"></div>
                </div>
            }
            {loading &&
                    <div className="overflow-auto">
                        {[...Array(21)].map((element, index) => (
                            <div key={index} className={`flex flex-col p-2 ${index % 2 !== 0 ? "bg-gray-50" : "bg-gray-100"} hover:bg-gray-300 cursor-pointer overflow-hidden`}>
                                <ListElement dummy/>
                            </div>
                        ))}
                    </div>
            }
        </div>
    );
}

export default List;
