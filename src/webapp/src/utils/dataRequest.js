import axios from 'axios';
const config = require('../config');

const dataRequests = async (endpoint, id = '', url = config.api.hostname, port = config.api.port) => {
    let data = [];
    let requestUrl = '';
    if (url) requestUrl += `${url}`;
    if (port) requestUrl += `:${port}`
    requestUrl += `/${endpoint}`;
    if (id) requestUrl += `/${id}`

    // Request the data from the api
    try {
        const response = await axios.get(requestUrl);
        data = response.data;
    } catch (error) {
        console.log("Error occoured while requesting " + requestUrl + ": ", error);
    }
    return data;
}

export default dataRequests;
