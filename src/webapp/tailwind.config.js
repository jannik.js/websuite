const colors = require('tailwindcss/colors');
const plugin = require('tailwindcss/plugin')

module.exports = {
  purge: [],
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        gray: colors.gray,
        orange: colors.orange,
        coolGray: colors.coolGray,
        lime: colors.lime,
        green: colors.green,
        emerald: colors.emerald,
        teal: colors.teal,
        cyan: colors.cyan,
        lightBlue: colors.sky,
      },
      height: {
        'screen-1/2': '50vh',
        'screen-1/3': 'calc(100vh / 3)',
        'screen-2/3': 'calc((100vh / 3) * 2)',
        'screen-1/4': '25vh',
        'screen-2/4': '50vh',
        'screen-3/4': '75vh',
        'screen-1/5': '20vh',
        'screen-2/5': '40vh',
        'screen-3/5': '60vh',
        'screen-4/5': '80vh',
        'screen-1/6': 'calc(100vh / 6)',
        'screen-2/6': 'calc((100vh / 6) * 2)',
        'screen-3/6': '50vh',
        'screen-4/6': 'calc((100vh / 6) * 4)',
        'screen-5/6': 'calc((100vh / 6) * 5)',
      },
      zIndex: {
        '-1': '-1',
      },
      animation: {
        'move-bg': 'move-bg 2s linear infinite',
        shimmer: 'shimmer 3s ease-out infinite',
      },
      keyframes: {
        'move-bg': {
          '0%': { 'background-position': '0 0' },
          '100%': { 'background-position': '150px 0' },
        },
        shimmer: {
          '100%': {
            transform: 'translateX(0%)',
            opacity: '0',
          }
        }
      },
    },
  },
  variants: {
    extend: {
      backgroundColor: ['active'],
      textColor: ['active'],
      borderWidth: ['focus'],
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
};
