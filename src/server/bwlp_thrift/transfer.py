import socket

class Transfer:

    def __init__(self, uploading, hostname, ti, file_size):
        # Init buffer variables
        self.read_buffer = bytearray()
        self.write_buffer = bytearray()

        # Initialize connection
        try:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.connect((hostname, int(ti.plainPort)))

            # Send upload / download
            if uploading:
                self.direction = bytearray('U', 'utf-8')
            else:
                self.direction = bytearray('D', 'utf-8')
            self.write_buffer += self.direction

        except socket.error:
            print(socket.error)

        # Send token
        self.send_key_value("TOKEN", ti.token)

        # Define chunk size to 16 mb || file_size
        self.chunk_size = 16 * 1024 * 1024
        if self.chunk_size > file_size:
            self.chunk_size = file_size
        self.start_offset = 0
        self.end_offset = self.chunk_size
        self.ti = ti
        self.total_transferred = 0
        self.file_size = file_size

    def send_key_value(self, key, value):
        if len(key) <= 0:
            return False
        message = key + '=' + value

        # Write length of message
        self.write_buffer += len(message).to_bytes(2, 'big')

        # Write message
        self.write_buffer += bytes(message, 'utf-8')

        return self.send_end_of_meta()

    def send_end_of_meta(self):
        try:
            # Append two null bytes
            self.write_buffer += bytes(2)
            self.socket.send(self.write_buffer)

            print("Send: ", self.write_buffer)

            self.write_buffer = bytearray()

        except socket.error:
            print("ERROR send_end_of_meta:")
            print(socket.error)
            return False

    def read_meta(self):
        result = ''
        # First two bytes contain the length
        try:
            # Read length in bytes
            meta_length = self.socket.recv(2)
            print("META_LENGTH: ", meta_length)

            # Convert from bytes to int
            meta_length = int.from_bytes(meta_length, 'big')
            meta_bytes = self.socket.recv(meta_length)

            print("META_BYTES: ", meta_bytes)

            self.read_end_of_meta()
            result = str(meta_bytes.decode('utf-8'))

            print("RESULT: ", result)

        except socket.error as error:
            print("ERROR read_meta:")
            print(error)
            return False

        return result

    def read_end_of_meta(self):
        try:
            read_bytes = self.socket.recv(2)

            print('END OF META BYTES: ', read_bytes)

            if len(read_bytes) != 2 or read_bytes[0] != 0x00 or read_bytes[1] != 0x00:
                print("ERROR read_end_of_meta:")
                print(read_bytes)
                return False
        except socket.error:
            print("ERROR read_end_of_meta:")
            print(socket.error)

    def update_offsets(self):
        if self.total_transferred == self.end_offset:
            self.start_offset = self.end_offset
            if self.end_offset + self.chunk_size > self.file_size:
                self.end_offset = self.file_size
            else:
                self.end_offset = self.start_offset + self.chunk_size

    def process_range_request(self):
        meta_data = self.read_meta()
        if not meta_data:
            return False

        # String RANGE=x:y expected split it to get start = x and end = y
        meta_kv = meta_data.split('=')
        if meta_kv[0] != 'RANGE':
            print("ERROR process_range_request:")
            print(meta_kv[0])
            return False
        rangeBounds = meta_kv[1].split(':')

        # Parse Bounds to int
        start = int(rangeBounds[0])
        end = int(rangeBounds[1])

        # Send confirmation
        self.send_key_value(start, end)
        self.start_offset = start
        self.end_offset = end
        self.update_offsets()
        return

    def send_range_request(self, start_offset, end_offset):
        # Send range request
        range_string = str(start_offset) + ':' + str(end_offset)
        self.send_key_value("RANGE", range_string)

        # Read confirmation
        meta = self.read_meta()

        # Test if they match
        full_range_string = "RANGE=" + range_string
        if meta != full_range_string:
            return False
        return end_offset - start_offset

    def read(self):
        _chunk_size = self.chunk_size
        if self.total_transferred == self.file_size:
            self.send_key_value("DONE", "")
            return False
        if self.total_transferred == 0 or self.total_transferred == self.end_offset:
            self.update_offsets()

            # No data cached, request from remote connection
            _chunk_size = self.send_range_request(self.start_offset, self.end_offset)
            if not _chunk_size:
                return False

        # Read the data
        read_bytes = b""
        while len(read_bytes) < _chunk_size:
            read_bytes += self.socket.recv(_chunk_size - len(read_bytes))

        print("CHUNK SIZE: ", _chunk_size)
        print("READ DATA LENGTH: ", len(read_bytes))
        # print("READ DATA: ", read_bytes)

        self.read_buffer += read_bytes
        self.total_transferred += len(read_bytes)

        print("TOTALTRANSFERED: ", self.total_transferred)

        return read_bytes

    def write(self, write_bytes):
        try:
            if self.total_transferred == 0 or self.total_transferred + len(write_bytes) > self.end_offset or self.total_transferred == self.end_offset:
                if self.write_buffer != 0:
                    self.socket.send(self.write_buffer)
                    self.write_buffer = bytearray()
                self.process_range_request()

            # Write the data
            self.write_buffer += write_bytes
            self.total_transferred += len(write_bytes)

            if self.total_transferred == self.file_size:
                if self.write_buffer != 0:
                    self.socket.send(self.write_buffer)
                self.read_end_of_meta()

        except socket.error:
            print("ERROR write:")
            print(socket.error)

        return "toto"
