import sys, glob, json

sys.path.append('bwlp_thrift/gen-py')
sys.path.insert(0, glob.glob('bwlp_thrift/lib*')[0])

from bwlp import MasterServer
from bwlp import SatelliteServer
from bwlp import ttypes

from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol

from thrift.TSerialization import serialize
from thrift.protocol.TJSONProtocol import TSimpleJSONProtocolFactory

from configparser import ConfigParser

from .transfer import Transfer

# Read config file for the tokens
config = ConfigParser()
config.read('config.ini')

class Server:
    def __init__(self, ip, port, server):
        self.ip = ip
        self.port = port
        self.server = server

    def create_client(self):
        socket = TSocket.TSocket(self.ip, self.port)
        transport = TTransport.TFramedTransport(socket)
        protocol = TBinaryProtocol.TBinaryProtocol(transport)
        client = self.server.Client(protocol)
        return transport, client

__satellite = Server(config['DEFAULT']['satellite_ip'], config['DEFAULT']['satellite_port'], SatelliteServer)
__master = Server(config['DEFAULT']['master_ip'], config['DEFAULT']['master_port'], MasterServer)

#__satellite_ip = '132.230.4.2'
#__satellite_port = 9090
#__master_ip = '132.230.4.16'
#__master_port = 9090

#__satellite_transport = TSocket.TSocket(__satellite_ip, __satellite_port)
#__satellite_transport = TTransport.TFramedTransport(__satellite_transport)
#__satellite_protocol = TBinaryProtocol.TBinaryProtocol(__satellite_transport)
#__satellite_client = SatelliteServer.Client(__satellite_protocol)

#__master_transport = TSocket.TSocket(__master_ip, __master_port)
#__master_transport = TTransport.TFramedTransport(__master_transport)
#__master_protocol = TBinaryProtocol.TBinaryProtocol(__master_transport)
#__master_client = MasterServer.Client(__master_protocol)

# Connect!
# transport.open()

# Close!
# transport.close()

# ______________________
# ___HELPER_FUNCTIONS___

def __thrift_to_json(thrift_object):
  return serialize(thrift_object, protocol_factory=TSimpleJSONProtocolFactory())

# ______________________
# ________TTPES_________
def get_sharemodes():
    return ttypes.ShareMode._VALUES_TO_NAMES

# ______________________
# ___SATELLITE_SERVER___

def get_list(datatype, session_token, page=0):
    data_list = []
    transport, client = __satellite.create_client()
    transport.open()

    try:
        if datatype == 'images':
            objects = client.getImageList(session_token, '', page)
        elif datatype == 'lectures':
            objects = client.getLectureList(session_token, page)
        elif datatype == 'users':
            objects = client.getUserList(session_token, page)
        elif datatype == 'operatingsystems':
            objects = client.getOperatingSystems()
        for obj in objects:
            data_list.append(json.loads(__thrift_to_json(obj).decode('utf-8')))
    except ttypes.TAuthorizationException:
        print('Session ID not valid')
        # return { 'error': 'Session ID not valid!' }

    transport.close()
    return data_list

def download_image(session_token, imageVersionId, file_size):
    transport, client = __satellite.create_client()
    transport.open()

    try:
        transfer_information = client.requestDownload(session_token, imageVersionId)
        print(transfer_information)
        print(transfer_information.token)
        transfer = Transfer(False, config['DEFAULT']['satellite_ip'], transfer_information, file_size)

        def read_stream():
            read_bytes = ''
            while True:
                read_bytes = transfer.read()
                if read_bytes == False:
                    break
                yield read_bytes
            yield b''

        # TODO Exception for canceld dl
        # client.cancelDownload(test.token)
        # client.cancelDownload(test2.token)
    except ttypes.TAuthorizationException:
        print('Session ID not valid')
    except Exception as e:
        print(e)

    transport.close()
    #return send_file(io.BytesIO(res), attachment_filename="test.xyz", mimetype='image/jpg')
    return read_stream()

def cancel_download(download_token):
    print('cancel_download:')
    transport, client = __satellite.create_client()
    transport.open()

    try:
        response = client.cancelDownload(download_token)
        print(response)
        print()
    except ttypes.TAuthorizationException:
        print('Session ID not valid')

    transport.close()

# Datatype = ('image' || 'lecture')
def get_details(session_token, resource_id, datatype):
    details = {}
    transport, client = __satellite.create_client()
    transport.open()

    try:
        if datatype == 'image':
           response = client.getImageDetails(session_token, resource_id)
        elif datatype == 'lecture':
           response = client.getLectureDetails(session_token, resource_id)
        else:
            return
        details = json.loads(__thrift_to_json(response).decode('utf-8'))
    except ttypes.TAuthorizationException:
        print('Session ID not valid')

    transport.close()
    return details

# ___________________
# ___MASTER_SERVER___

def get_public_images(master_token, page=0):
    image_list = []
    transport, client = __master.create_client()
    transport.open()

    try:
        images = client.getPublicImages(master_token, page)
        for image in images:
            image_list.append(json.loads(__thrift_to_json(image).decode('utf-8')))
    except ttypes.TAuthorizationException:
        print('Session ID not valid')

    transport.close()
    return image_list
