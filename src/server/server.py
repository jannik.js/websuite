import json, io, os, requests, socket, sys, time, uuid

from configparser import ConfigParser
from flask import Flask, request, Response, render_template, send_from_directory
from flask_restful import Api, Resource
from flask_cors import CORS
from werkzeug.datastructures import Headers

from bwlp_thrift import bwlp_servers

from base64 import b64encode
from Crypto import Random
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad

import mysql.connector as mysql


# Initialization
server = Flask(__name__, static_folder='build/static', template_folder='build')
cors = CORS(server)
api = Api(server)

# Read config file for the tokens
config = ConfigParser()
config.read('config.ini')
client_token = config['DEFAULT']['client_token']
masterserver_token = config['DEFAULT']['masterserver_token']

# Setup database
database = mysql.connect(
    host = config['DEFAULT']['db_host'],
    user = config['DEFAULT']['db_user'],
    password = config['DEFAULT']['db_password'],
    database = 'websuite'
)

def init_db():
    db = database.cursor()
    # Setup db if not exists
    db.execute("CREATE TABLE progresses ("
               "sessionid VARCHAR(36) NOT NULL PRIMARY KEY, "
               "lectureid VARCHAR(36) NOT NULL, "
               "userid VARCHAR(36) NOT NULL, "
               "guactoken TEXT, "
               "clientip VARCHAR(39), "
               "state VARCHAR(255), "
               "progress TEXT, "
               "starttime BIGINT, "
               "finishtime BIGINT)")
    database.commit()
    db.close()
    print("... successfully created progresses")
try:
    print("Try creating tables ...")
    init_db()
except mysql.Error as err:
    print(err)

# Serve react app
@server.route('/')
def react():
    return render_template('index.html')

# Serve react progress path
@server.route('/progress/<string:id>')
def progress(id):
    return render_template('index.html')

# Serve public files
@server.route('/<path:path>')
def public(path):
    return send_from_directory('build', path)

# Routes / Resources
class PublicImages(Resource):
    def get(self):
        return bwlp_servers.get_public_images(masterserver_token)

api.add_resource(PublicImages, '/public_images')

class Images(Resource):
    def get(self):
        return bwlp_servers.get_list('images', client_token)

api.add_resource(Images, '/images')

class Image(Resource):
    def get(self, image_id):
        return bwlp_servers.get_details(client_token, image_id, 'image')

api.add_resource(Image, '/images/<string:image_id>')

class Download_Image(Resource):
    def get(self, image_base_id, image_version_id):
        image = bwlp_servers.get_details(client_token, image_base_id, 'image')
        image_version = next((img for img in image['versions'] if img['versionId'] == image_version_id), None)

        # Set filesize header, so that the browser knows the size of the downloaded file
        headers = Headers()
        headers.add('Content-Length', image_version['fileSize'])

        # Return the download stream
        return Response(bwlp_servers.download_image(client_token, image_version_id, image_version['fileSize']), headers=headers, mimetype='application/x-vmdk')

api.add_resource(Download_Image, '/images/<string:image_base_id>/versions/<string:image_version_id>')

class Cancel_Download(Resource):
    def get(self, download_token):
        return bwlp_servers.cancel_download(download_token)

api.add_resource(Cancel_Download, '/downloads/<string:download_token>/cancel')

class Upload_Image(Resource):
    def post(self, image_base_id):
        uploaded_file = request.files['file']
        print(uploaded_file)
        if uploaded_file.filename != '':
            uploaded_file.save('./uploads/' + uploaded_file.filename)
        return "Success"
api.add_resource(Upload_Image, '/images/<string:image_base_id>/versions/upload')

from random import randint
class Edit_Progress(Resource):
    def get(self, edit_session_id):
        # return { 'progress': randint(0,100), 'sessionid': edit_session_id, 'state': 'TRANFER' }

        # Get the edit info corresponding to the edit session id
        sql = "SELECT state, progress FROM progresses WHERE sessionid = %s"
        values = (edit_session_id,)
        db = database.cursor()
        db.execute(sql, values)
        db_result = db.fetchone()
        db.close()

        print(db_result)
        progress = db_result[1]
        if (progress):
            progress = json.loads(db_result[1])
        return { 'state': db_result[0], 'progress': progress }

    def post(self, edit_session_id):
        # Update db entry with given edit session id
        sql = "UPDATE progresses SET state = %s, progress = %s WHERE sessionid = %s"
        data = json.loads(request.data.decode('utf-8'))
        values = (data['state'], json.dumps(data), edit_session_id)
        db = database.cursor()
        db.execute(sql, values)
        database.commit()
        db.close()

        return "Success"
api.add_resource(Edit_Progress, '/edits/<string:edit_session_id>/progress')

class Save_Edit(Resource):
    def get(self, edit_session_id):
        print("Saving session (trigger scp task)")

        # Check if it's a valid edit session id
        sql = 'SELECT * from progresses WHERE sessionid = %s'
        values = (edit_session_id,)
        db = database.cursor()
        db.execute(sql, values)
        result = db.fetchall()
        db.close()

        if len(result) == 0:
            return "Invalid edit id"
        print("Valid edit session, start save request")

        # Trigger scp session task via the slx-admin backend
        payload = { 'do': 'remoteaccess', 'action': 'scp', 'editid': edit_session_id }
        slxadmin_api_url = config['DEFAULT']['slx_admin_api_url']
        try:
            r = requests.get(url = slxadmin_api_url, params = payload)
            result = r.text
        except Exception:
            message = "Cannot trigger scp tast: "
            if r.text:
                message += r.text
            return message, 500

        return result
api.add_resource(Save_Edit, '/edits/<string:edit_session_id>/save')

class Lectures(Resource):
    def get(self):
        return bwlp_servers.get_list('lectures', client_token)

api.add_resource(Lectures, '/lectures')

class Lecture(Resource):
    def get(self, lecture_id):
        return bwlp_servers.get_details(client_token, lecture_id, 'lecture')

api.add_resource(Lecture, '/lectures/<string:lecture_id>')

class Users(Resource):
    def get(self):
        return bwlp_servers.get_list('users', client_token)

api.add_resource(Users, '/users')

class OperatingSystems(Resource):
    def get(self):
        return bwlp_servers.get_list('operatingsystems', client_token)

api.add_resource(OperatingSystems, '/operatingsystems')

class ShareMode(Resource):
    def get(self):
        return bwlp_servers.get_sharemodes()

api.add_resource(ShareMode, '/sharemodes')

class Remote(Resource):
    def get(self, lecture_id):
        # Check if user is allowed to start edit process

        # Generate edit id
        session_id = str(uuid.uuid4())

        # Reserve client via the slx-admin api and get ip/port of the client used for the remote connection
        payload = { 'do': 'remoteaccess', 'action': 'reserve', 'lectureid': lecture_id, 'editid': session_id }
        slxadmin_api_url = config['DEFAULT']['slx_admin_api_url']
        try:
            r = requests.get(url = slxadmin_api_url, params = payload)
            client = r.json()
        except Exception:
            message = "Cannot reserve client: "
            if r.text:
                message += r.text
            return message, 500

        # Generate guacamole token
        iv = Random.get_random_bytes(16)
        key = bytes(config['DEFAULT']['guacamole_secret'], 'utf-8')

        cipher = AES.new(key, AES.MODE_CBC, iv)

        # Object for the remote connection
        vnc_connection = {
            'connection': {
                'type': 'vnc',
                'settings': {
                    'hostname': client['clientip'],
                    #'port': int(client['remote']['vncport']),
                    'port': int(client['vncport']),
                    'username': '',
                    #'password': client['remote']['password']
                    'password': client['password']
                }
            }
        }

        # Encrypt remote connection object
        vnc_string = json.dumps(vnc_connection)
        vnc_string_bytes = vnc_string.encode('utf-8')
        ciphertext = cipher.encrypt(pad(vnc_string_bytes, AES.block_size))

        # Object with all informations needed by the guacamole-lite server
        data = {
            'iv': b64encode(iv).decode('utf-8'),
            'value': b64encode(ciphertext).decode('utf-8')
        }

        # Encrypt the data object to get the guacamole token
        data_string = json.dumps(data)
        data_string_bytes = data_string.encode('utf-8')
        data_string_b64 = b64encode(data_string_bytes)
        data_encrypted = data_string_b64.decode('utf-8')

        # Create db entry
        sql = "INSERT INTO progresses (sessionid, lectureid, userid, guactoken, clientip, state, starttime) VALUES (%s, %s, %s, %s, %s, %s, %s)"
        values = (session_id, lecture_id, client_token, data_encrypted, client['clientip'], "INITIALIZED", int(time.time()))
        db = database.cursor()
        db.execute(sql, values)
        database.commit()
        db.close()

        return data_encrypted
api.add_resource(Remote, '/remote/<string:lecture_id>')

class RemoteLogin(Resource):
    def get(self, lecture_id):
        # For testing only :::: hardcoded stuff
        username = config['DEFAULT']['remote_client_user']
        password = config['DEFAULT']['remote_client_password']
        lectureUUID = lecture_id
        host = '10.21.9.200' # 10.21.9.200 for testing
        port = 7551

        # Get edit session id from the db
        db = database.cursor()
        sql = 'SELECT sessionid from progresses WHERE lectureid = %s AND clientip = %s'
        variables = (str(lecture_id), str(host))
        db.execute(sql, variables)
        db_result = db.fetchall()
        db.close()
        session_id = db_result[0][0]

        # Connect to the slxgreeter via rpc login
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.settimeout(1)
            s.connect((host, port))
            s.settimeout(None)
            version = 1
            version_bytes = bytes([version >> 8, version & 0xFF])
            data_string = str(username) + '\n' + str(password) + '\n' + '1920x1080' + '\n' + lectureUUID + '\n' + session_id
            encoded_bytes = b64encode(data_string.encode('utf-8'))
            data_bytes = version_bytes + encoded_bytes
            s.sendall(data_bytes)
            data = s.recv(1024)
            print(data)
        return 'done.'
api.add_resource(RemoteLogin, '/remotelogin/<string:lecture_id>')

# Run Server
if __name__ == '__main__':
    server.run(host='0.0.0.0', port=config['DEFAULT']['port'], debug=False)
