const GuacamoleLite = require('guacamole-lite');
const http = require('http');
const config = require('./config');

const websocketOptions = {
        // Accept connections to this port
        port: config.port,
};

const guacdOptions = {
        // Guacd port
        port: config.guacd.port,
};

const clientOptions = {
        crypt: {
                cypher: config.cypher,
                key: config.secret,
        },
        log: {
                verbose: true,
        },
};

const guacServer = new GuacamoleLite(websocketOptions, guacdOptions, clientOptions);
