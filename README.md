# WebSuite

In scope of my master's thesis i am creating a webapp version of the dozmod module of bwLehrpool called Web-Suite

___

## Thrift
Original thrift file [bwlp.thift](https://git.openslx.org/bwlp/master-sync-shared.git/tree/src/main/thrift/bwlp.thrift)

___

## Webapp
https://gitlab.com/jannik.js/websuite/-/tree/master/src/webapp

### Working Directory
`websuite/src/webapp/`

## Copy config template and fill out all <>
```
cp src/config.json.template src/config.json
```

```json
{
    "api": {
        "hostname": "<HOST>",
        "port": <PORT>
    },
    "remote": {
        "serverurl": "<SERVERURL>"
    }
}
```
> api: Python backend hostname `http://<IP>` and PORT\
> remote: guacamole-lite url `ws://<IP>:<PORT>`

### Start Development Server
```
npm start
```

### Build Production Webapp
```
npm run build
```
This command automatically builds the tailwind css files.

After building the webapp successfully, the **python backend has to be restarted** (which serves the webapp)

___

## Server
https://gitlab.com/jannik.js/websuite/-/tree/master/src/server

### Working Directory
`websuite/src/server/`

### Copy config template and fill out all <>
```
cp config.ini.template config.ini
```

### Link production build in server
```
ln -s ../webapp/build build
```

### Create pipenv shell
```
pipenv shell
```

### Start backend python server (inside pipenv shell)
```
python server.py
```

___

## Guacamole Lite
https://gitlab.com/jannik.js/websuite/-/tree/master/src/guacamole-lite

### Working Directory
`websuite/src/guacamole-lite`

### Start
```
node index.js
```